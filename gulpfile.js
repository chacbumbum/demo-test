const jshint = require('gulp-jshint');
const eslint = require('gulp-eslint');
const standard = require('gulp-standard');
const gulp = require('gulp');

const jsPath = ['src/**/*.js'];

gulp.task('es5lint', () => {
    return gulp.src(jsPath)
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish', { beep: true }))
        .pipe(jshint.reporter('fail'));
});

gulp.task('es6lint', () => {
    // ESLint ignores files with "node_modules" paths.
    // So, it's best to have gulp ignore the directory as well.
    // Also, Be sure to return the stream from the task;
    // Otherwise, the task may end before the stream has finished.
    return gulp.src(jsPath)
        // eslint() attaches the lint output to the "eslint" property
        // of the file object so it can be used by other modules.
        .pipe(eslint())
        // eslint.format() outputs the lint results to the console.
        // Alternatively use eslint.formatEach() (see Docs).
        .pipe(eslint.format())
        // To have the process exit with an error code (1) on
        // lint error, return the stream and pipe to failAfterError last.
        .pipe(eslint.failAfterError());
});

gulp.task('standard', () => {
    return gulp.src(jsPath)
        .pipe(standard())
        .pipe(standard.reporter('default', {
            breakOnError: true,
            quiet: true
        }));
});