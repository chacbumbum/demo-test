### Test Git CI/CD

### TL;DR
```shell
    npm install
```

Standard testing
```shell
    npm run standard
```

ES5 testing
```shell
    npm run es5lint
```

ES6 testing
```shell
    npm run es6lint
```

> Code linter in `src` folder